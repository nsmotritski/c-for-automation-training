﻿using System;

namespace NET01._1_SumOfNumbers
{
    internal class Program
    {
        /*
         *Постановка задачи

            1. При старте приложение запрашивает у пользователя два целых числа: a и b. 
            Затем приложение считает и выводит сумму всех целых чисел в диапазоне от a до b (включительно), 
            которые делятся без остатка или на 3, или на 5, но не делятся на 20. 
            Разработать консольное приложение, реализующее указанный функционал. 
         */
        internal static void Main(string[] args)
        {
            int a, b, sum = 0;
            Console.WriteLine("Enter two numbers: a and b. a must be greated than b:");
            a = int.Parse(Console.ReadLine());
            b = int.Parse(Console.ReadLine());
            for (int i = a; i < b + 1; i++)
            {
                if (((i % 3 == 0) || (i % 5 == 0)) && (i % 20 != 0))
                {
                    sum += i;
                }
            }
            Console.WriteLine("The sum of numbers between a and b than can be divided by 3 or 5, but cannot be divided by 20, is: " + sum);
            Console.ReadKey();
        }
    }
}
