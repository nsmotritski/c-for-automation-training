﻿using System;
using System.Linq;

namespace NET01._2_WithTuples
{
    internal class Program
    {
        /*
         *Постановка задачи

            1. Пользователь вводит количество элементов массива, а затем в цикле – сами элементы. 
            Каждый элемент массива – это строка из латинских букв в нижнем регистре (корректность ввода можно не проверять). 
            После ввода пользователя программа печатает исходный массив. 
            Затем программа сортирует массив, располагая стоки по уменьшению числа гласных букв в них (гласные - это a, e, i, o, u, y). 
            Строки без гласных вообще удаляются из отсортированного массива. Важно: при решении задачи используйте только массивы! 
            Списки, словари, LINQ применить не разрешается! 
         */
        internal static void Main(string[] args)
        {
            Console.WriteLine("Input number of elements of the string array: ");
            var numberOfElements = int.Parse(Console.ReadLine());
            var tuples = new Tuple<string, int>[numberOfElements];
            var numberOfStringsWithNoVowels = 0;
            var str = "";

            Console.WriteLine("Input elements of the string array in lowercase, one by one:");
            for (int i = 0; i < numberOfElements; i++)
            {
                str = Console.ReadLine();
                tuples[i] = new Tuple<string, int>(str, CountNumberOfVowelLetters(str));
                if (CountNumberOfVowelLetters(str) == 0)
                {
                    numberOfStringsWithNoVowels += 1;
                }
            }
            Console.WriteLine();
            Console.WriteLine("Initial array:");
            PrintTuplesArray(tuples);

            //Sort array of tuples (BubbleSort)
            for (int i = 0; i < numberOfElements; i++)
            {
                for (int j = i + 1; j < numberOfElements; j++)
                {
                    if (tuples.ElementAt(i).Item2 > tuples.ElementAt(j).Item2)
                    {
                        var tempTuple = tuples[i];
                        tuples[i] = tuples[j];
                        tuples[j] = tempTuple;
                    }
                }
            }

            if (numberOfStringsWithNoVowels == 0)
            {
                //Printing initial sorted array
                Console.WriteLine("Resulting array: ");
                PrintTuplesArray(tuples);
            }
            else
            {
                //Creating new array of tuples with number of elements equal to the number of elements from initial array minus numberOfStringsWithNoVowels
                var numberOfElementsOfResultingArray = numberOfElements - numberOfStringsWithNoVowels;
                var resultingArrayOfTuples = new Tuple<string, int>[numberOfElementsOfResultingArray];

                //Assigning string elements to the new array only if a string contains vowel letters
                for (int i = numberOfStringsWithNoVowels; i < numberOfElements; i++)
                {
                    resultingArrayOfTuples[i - numberOfStringsWithNoVowels] = tuples[i];
                }

                Console.WriteLine("Resulting array: ");
                PrintTuplesArray(resultingArrayOfTuples);
            }

            Console.ReadKey();
        }

        private static void PrintTuplesArray(Tuple<string, int>[] tuples)
        {
            foreach (var tuple in tuples)
            {
                Console.WriteLine(tuple.Item1 + " , " + tuple.Item2.ToString());
            }
            Console.WriteLine();
        }

        private static int CountNumberOfVowelLetters(string str)
        {
            var sum = 0;
            var vowelLetters = new string[] { "a", "e", "i", "o", "u", "y" };

            foreach (var letter in vowelLetters)
            {
                sum += str.Length - str.Replace(letter, "").Length;
            }
            return sum;
        }
    }
}
