﻿using System;

namespace NET01._2_WorkingWithArrays
{
    internal class Program
    {
        /*
         *Постановка задачи

            1. Пользователь вводит количество элементов массива, а затем в цикле – сами элементы. 
            Каждый элемент массива – это строка из латинских букв в нижнем регистре (корректность ввода можно не проверять). 
            После ввода пользователя программа печатает исходный массив. 
            Затем программа сортирует массив, располагая стоки по уменьшению числа гласных букв в них (гласные - это a, e, i, o, u, y). 
            Строки без гласных вообще удаляются из отсортированного массива. Важно: при решении задачи используйте только массивы! 
            Списки, словари, LINQ применить не разрешается! 
         */
        internal static void Main(string[] args)
        {
            Console.WriteLine("Input number of elements of the string array: ");
            var numberOfElements = int.Parse(Console.ReadLine());
            var arrayOfStrings = new string[numberOfElements];
            var numberOfVowelLetters = new int[numberOfElements];
            var numberOfStringsWithNoVowels = 0;

            Console.WriteLine("Input elements of the string array in lowercase, one by one:");
            for (int i = 0; i < numberOfElements; i++)
            {
                arrayOfStrings[i] = Console.ReadLine();
            }
            Console.WriteLine();
            Console.WriteLine("Initial array:");
            PrintStringArray(arrayOfStrings);

            //Count number of vowel letters for each of the strings in the initial array and put the number in an array of int
            for (int i = 0; i < numberOfElements; i++)
            {
                numberOfVowelLetters[i] = CountNumberOfVowelLetters(arrayOfStrings[i]);
                if (numberOfVowelLetters[i] == 0)
                {
                    numberOfStringsWithNoVowels += 1;
                }
            }

            //Sort both arrays (BubbleSort)
            for (int i = 0; i < numberOfElements; i++)
            {
                for (int j = i + 1; j < numberOfElements; j++)
                {
                    if (numberOfVowelLetters[i] > numberOfVowelLetters[j])
                    {
                        var tempNumber = numberOfVowelLetters[i];
                        numberOfVowelLetters[i] = numberOfVowelLetters[j];
                        numberOfVowelLetters[j] = tempNumber;
                        var temrString = arrayOfStrings[i];
                        arrayOfStrings[i] = arrayOfStrings[j];
                        arrayOfStrings[j] = temrString;
                    }
                }
            }

            if (numberOfStringsWithNoVowels == 0)
            {
                //Printing initial sorted array
                Console.WriteLine("Resulting array: ");
                PrintStringArray(arrayOfStrings);
            }
            else
            {
                //Creating new array with number of elements equal to the number of elements from initial array minus numberOfStringsWithNoVowels
                var numberOfElementsOfResultingArray = numberOfElements - numberOfStringsWithNoVowels;
                var resultingArrayOfStrings = new string[numberOfElementsOfResultingArray];

                //Assigning string elements to the new array only if a string contains vowel letters
                for (int i = numberOfStringsWithNoVowels; i < numberOfElements; i++)
                {
                    resultingArrayOfStrings[i - numberOfStringsWithNoVowels] = arrayOfStrings[i];
                }

                Console.WriteLine("Resulting array: ");
                PrintStringArray(resultingArrayOfStrings);
            }

            Console.ReadKey();
        }

        private static void PrintStringArray(string[] arrayOfStrings)
        {
            foreach (var s in arrayOfStrings)
            {
                Console.WriteLine(CountNumberOfVowelLetters(s) +", " + s);
            }
            Console.WriteLine();
        }

        private static int CountNumberOfVowelLetters(string str)
        {
            var sum = 0;
            var vowelLetters = new string[] { "a", "e", "i", "o", "u", "y" };

            foreach (var letter in vowelLetters)
            {
                sum += str.Length - str.Replace(letter, "").Length;
            }
            return sum;
        }
    }
}
