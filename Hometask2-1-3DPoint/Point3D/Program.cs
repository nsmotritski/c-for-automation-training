﻿using System;

namespace Point3D
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var point = new Point3D(1, 2, 3);
            var pointWithMass = new Point3D(1, 2, 3, -0.15);
            Console.WriteLine("Test for point with negative mass: " + pointWithMass.Mass);
            point[0] = 8;
            point[2] = 9;
            //test for set wrong coordinate index
            point[3] = 10;
            //test for get wrong coordinate index
            Console.WriteLine("Test for get wrong coordinate index. Returned value: " + point[5]);
            Console.WriteLine("Print Indexer:");
            for (var i = 0; i < 3; i++)
            {
                Console.WriteLine(point[i]);
            }
            Console.WriteLine("Print Coordinates:");
            Console.WriteLine("X: " + point.X + ", Y: " + point.Y + ", Z: " + point.Z);

            Console.WriteLine("Test for IsZero method (negative): " + new Point3D(1, 2, 3).IsZero());
            Console.WriteLine("Test for IsZero method (positive): " + new Point3D(0, 0, 0).IsZero());

            var pointA = new Point3D(1, 2, 3);
            var pointB = new Point3D(2, 3, 5);

            Console.WriteLine("Test for GetDistance method: " + pointA.GetDistance(pointB));

            Console.ReadKey();
        }
    }
}