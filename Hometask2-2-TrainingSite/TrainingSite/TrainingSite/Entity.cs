﻿using System;

namespace TrainingSite
{
    public abstract class Entity
    {
        private string _textDescription;
        private const int MaxLength = 256;

        public Guid Id { get; set; }

        public string TextDescription
        {
            get => _textDescription;
            set
            {
                if (value == null)
                {
                    _textDescription = null;
                }
                else
                {
                    if (value.Length > MaxLength)
                    {
                        throw new ArgumentOutOfRangeException("TextDescription", "Text description must be less or equal to 256 characters");
                    }
                    _textDescription = value;
                }
            }
        }

        protected Entity(string description = null)
        {
            Id = Guid.NewGuid();
            TextDescription = description;
        }

        public override string ToString()
        {
            if (!string.IsNullOrEmpty(TextDescription))
            {
                return TextDescription;
            }
            return string.Empty;
        }

        public override bool Equals(Object obj)
        {
            if (!(obj is Entity))
            {
                return false;
            }
            return Id == ((Entity)obj).Id;
        } 

        public override int GetHashCode() { return Id.GetHashCode(); }
    }
}
