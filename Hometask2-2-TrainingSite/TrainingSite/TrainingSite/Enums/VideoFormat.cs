﻿namespace TrainingSite
{
    public enum VideoFormat
    {
        Unknown,
        Avi,
        Mp4,
        Flv
    }
}
