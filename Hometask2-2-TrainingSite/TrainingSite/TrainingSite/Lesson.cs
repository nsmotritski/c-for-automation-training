﻿namespace TrainingSite
{
    public class Lesson  : Entity
    {
        public Material[] Materials { get; set; }

        public Lesson(string textDescription = null, params Material[] materials)
        {
            Materials = new Material[materials.Length];
            for (var i = 0; i < materials.Length; i++)
            {
                Materials[i] = materials[i];
            }
            TextDescription = textDescription;
        }

        public LessonKind GetLessonType()
        {
            foreach (var trainingMaterial in Materials)
            {
                if (trainingMaterial is Video)
                {
                    return LessonKind.VideoLesson;
                }
            }
            return LessonKind.TextLesson;
        }
    }
}
