﻿using System;

namespace TrainingSite
{
    public class Link : Material
    {
        private string _contentUri;

        public string ContentUri
        {
            get => _contentUri;
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentOutOfRangeException("ContentUri", "Content should not be empty or null");
                }
                _contentUri = value;
            }
        }

        public LinkType LinkType { get; set; }

        public Link(string linkDescription,LinkType linkType = LinkType.Html, string contentUri = "")
        {
            TextDescription = linkDescription;
            ContentUri = contentUri;
            LinkType = linkType;
        }
    }
}
