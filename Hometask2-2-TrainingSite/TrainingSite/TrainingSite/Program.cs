﻿using System;

namespace TrainingSite
{
    public class Program
    {
        /*
         * Описание

            При создании сайта тренингов возникла необходимость в описании следующих сущностей. 
            Отдельное занятие тренинга (Lesson) включает набор из нескольких материалов тренинга (Material). 
            Материал тренинга бывает трёх видов: текстовый материал (Text), видеоматериал(Video), ссылка на сетевой ресурс(Link). 
            Текстовый материал хранит произвольный непустой текст. Видеоматериал содержит URI видеоконтента (непустая строка), 
            URI картинки-заставки, указание на формат видео (значение из фиксированного набора: Unknown, Avi, Mp4, Flv). 
            Ссылка хранит URI контента (непустая строка), указание на тип ссылки (значение из фиксированного набора: Unknown, Html, Image, Audio, Video). 
            Все сущности (включая занятие тренинга) обладают уникальным идентификатором1 и содержат текстовое описание (возможно пустое или null) 
            длиной до 256 символов.

            Постановка задачи

            1. Создать набор классов, соответствующих сущностям в описании (сущности выделены жирным шрифтом)

            2. В классе, соответствующем занятию тренинга, предусмотреть метод, возвращающий вид занятия. 
                Если занятие содержит хотя бы один видеоматериал, то его вид – VideoLesson, иначе – TextLesson (это элементы перечисления LessonKind).

            3. Метод ToString() в созданных классах должен возвращать текстовое описание сущности.

            4. Переопределить метод сравнения для сущностей. Две сущности считаются равными, если совпадают их уникальные идентификаторы.
         */
        public static void Main(string[] args)
        {
            var link1 = new Link("link1 description", contentUri: "link1 content");
            var text1 = new Text("text1 description");
            var video1 = new Video("video1 description");
            var lesson1 = new Lesson("Lesson 1 description", link1, text1, video1);
            var lesson2 = new Lesson("Lesson 2 description", link1, text1);

            // ToString() tests
            Console.WriteLine("Test ToString() method for Lesson: " + lesson1);
            Console.WriteLine("Test ToString() method for Link: " + link1);
            Console.WriteLine("Test ToString() method for Text: " + text1);
            Console.WriteLine("Test ToString() method for Video: " + video1);
            //separator
            Console.WriteLine();
            // Guid tests
            Console.WriteLine("Test Guid for Lesson1: " + lesson1.Id);
            Console.WriteLine("Test Guid for Link: " + link1.Id);
            Console.WriteLine("Test Guid for Text: " + text1.Id);
            Console.WriteLine("Test Guid for Video: " + video1.Id);
            var lesson3 = new Lesson("Lesson 3 description", link1);
            Console.WriteLine("Test Guid for Lesson3: " + lesson3.Id);
            //separator
            Console.WriteLine();
            // GetLessonType() tests
            Console.WriteLine("Lesson type of Lesson1: " + lesson1.GetLessonType());
            Console.WriteLine("Lesson type of Lesson2: " + lesson2.GetLessonType());
            //separator
            Console.WriteLine();
            // Equals() tests
            lesson3.Id = lesson1.Id;
            Console.WriteLine("Test Guid for Lesson3: " + lesson3.Id);
            Console.WriteLine("Lesson1 equals to Lesson2: " + lesson1.Equals(lesson2));
            Console.WriteLine("Lesson1 equals to Lesson3: " + lesson1.Equals(lesson3));
            //separator
            Console.WriteLine();
            // Training Materials attributes tests
            Console.WriteLine("Lesson1 material1 text description: " + lesson1.Materials[0]);
            Console.WriteLine("Lesson1 material1 text description: " + lesson1.Materials[0].Id);
            Console.WriteLine("Lesson1 material2 text description: " + lesson1.Materials[1]);
            Console.WriteLine("Lesson1 material2 text description: " + lesson1.Materials[1].Id);
            Console.WriteLine("Lesson1 material3 text description: " + lesson1.Materials[2]);
            Console.WriteLine("Lesson1 material3 text description: " + lesson1.Materials[2].Id);

            Console.ReadKey();
        }
    }
}
