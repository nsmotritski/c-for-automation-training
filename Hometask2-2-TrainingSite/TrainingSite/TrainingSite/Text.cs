﻿using System;

namespace TrainingSite
{
    public class Text : Material
    {
        private string _textMaterial;
        private const string DefaultText = "default text";

        public string TextMaterial
        {
            get => _textMaterial;
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentOutOfRangeException("TextMaterial", "TextMaterial should not be empty or null");
                }
                _textMaterial = value;
            }
        }

        public Text(string description, string material = DefaultText)
        {
            TextDescription = description;
            TextMaterial = material;
        }
    }
}
