﻿namespace TrainingSite
{
    public class Video : Material
    {
        public string VideoContentUri { get; set; }
        public string PreviewPictureUri { get; set; }
        public VideoFormat VideoFormat { get; set; }
        private const string DefaultVideoContentUri = "youtube.com";
        private const string DefaultPreviewPictureUri = "defaultpreviewpicture.com";

        public Video(string description, string videocontentUri = DefaultVideoContentUri, string previewPictureUri = DefaultPreviewPictureUri, VideoFormat videoFormat = VideoFormat.Avi)
        {
            TextDescription = description;
            VideoContentUri = videocontentUri;
            PreviewPictureUri = previewPictureUri;
            VideoFormat = videoFormat;
        }
    }
}
