﻿namespace NET01._5_Stack
{
    public struct Complex
    {
        public decimal Re { get; set; }
        public decimal Im { get; set; }

        public Complex(decimal re = default(decimal), decimal im = default(decimal))
        {
            Re = re;
            Im = im;
        }

        public override string ToString()
        {
            return "(" + Re + ", " + Im + ")";
        }
    }
}
