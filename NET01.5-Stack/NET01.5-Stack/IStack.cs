﻿namespace NET01._5_Stack
{
    public interface IStack<T>
    {
        void Push(T item);
        T Pop();
        bool IsEmpty();
    }
}
