﻿namespace NET01._5_Stack
{
    public static class IStackExtension
    {
        public static IStack<T> Reverse<T>(this IStack<T> stack) 
        {
            var reverseStack = new Stack<T>();
            while (!stack.IsEmpty())
            {
                reverseStack.Push(stack.Pop());
            }
            return reverseStack;
        }
    }
}
