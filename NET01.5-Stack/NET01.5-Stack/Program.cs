﻿using System;

namespace NET01._5_Stack
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var complexStack = new Stack<Complex>();
            for (int i = 0; i < 12; i++)
            {
                complexStack.Push(new Complex(i, i - 1));
            }
            Console.WriteLine(complexStack.ToString());
            Console.WriteLine();
            Console.WriteLine("Element Popped: " + complexStack.Pop());
            Console.WriteLine("Element Popped: " + complexStack.Pop());
            Console.WriteLine();
            Console.WriteLine(complexStack.ToString());
            Console.WriteLine();
            Console.WriteLine("Reverse Stack: ");
            var reverseStack = complexStack.Reverse();
            Console.WriteLine(reverseStack.ToString());

            Console.ReadKey();
        }
    }
}
