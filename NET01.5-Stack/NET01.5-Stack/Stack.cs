﻿using System;

namespace NET01._5_Stack
{
    public class Stack<T> : IStack<T>
    {
        private T[] _items;
        private const int StackInitialSpace = 5;
        private const int StackIncreaseStep = 10;

        private int LastIndex { get; set; }

        public Stack()
        {
            LastIndex = 0;
            _items = new T[StackInitialSpace];
        }

        public void Push(T item)
        {
            if (IsFull())
            {
                Resize();
            }
            _items[LastIndex] = item;
            LastIndex += 1;
        }

        public T Pop()
        {
            if (IsEmpty())
            {
                throw new NullReferenceException(message: "Stack is empty!");
            }

            var returnValue = _items[LastIndex - 1];
            _items[LastIndex - 1] = default(T);
            LastIndex -= 1;
            return returnValue;
        }

        public bool IsFull()
        {
            return LastIndex == _items.Length;
        }

        public bool IsEmpty()
        {
            return LastIndex == 0;
        }

        private void Resize()
        {
            var largerStack = new T[LastIndex + StackIncreaseStep];
            for (var i = 0; i < _items.Length; i++)
            {
                largerStack[i] = _items[i];
            }
            _items = largerStack;
        }

        public override string ToString()
        {
            var stackString = string.Empty;
            for (var i = 0; i < LastIndex; i++)
            {
                stackString += _items[i] + Environment.NewLine;
            }

            return stackString;
        }
    }
}
