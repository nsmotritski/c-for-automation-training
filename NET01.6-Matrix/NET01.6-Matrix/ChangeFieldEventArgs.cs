﻿using System;

namespace NET01._6_Matrix
{
    public class ChangeFieldEventArgs : EventArgs
    {
        public int I { get; }
        public int J { get; }
        public int OldValue { get; }
        public int NewValue { get; }

        public ChangeFieldEventArgs(int i, int j, int old, int @new)
        {
            OldValue = old;
            NewValue = @new;
            I = i;
            J = j;
        }

        public override string ToString()
        {
            return "Element [" + I + ", " + J + "] changed: OldValue = " + OldValue + ", NewValue = " + NewValue;
        }

    }
}
