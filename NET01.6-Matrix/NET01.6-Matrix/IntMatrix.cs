﻿using System;

namespace NET01._6_Matrix
{
    public class IntMatrix : Matrix<int>
    {
        public IntMatrix(int size) : base(size) { }


        public void PopulateIntMatrix()
        {
            Random randomNumber = new Random();
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    matrix[i, j] += randomNumber.Next(0, 9);
                }
            }
        }

        public static IntMatrix operator +(IntMatrix a, IntMatrix b)
        {
            if (a.Size <= 0 || b.Size <= 0)
            {
                throw new MatrixException("Must be > 0", nameof(a.Size));
            }
            if (a.Size != b.Size)
            {
                throw new MatrixException("Sizes of the matrixes must be equal", nameof(a.Size));
            }
            var matrixSum = new IntMatrix(a.Size);
            for (int i = 0; i < a.Size; i++)
            {
                for (int j = 0; j < a.Size; j++)
                {
                    matrixSum[i,j] = a[i, j] + b[i, j];
                }
            }
            return matrixSum;
        }

        public static IntMatrix operator *(IntMatrix a, int x)
        {
            var matrixMultiplied = new IntMatrix(a.Size);
            for (int i = 0; i < a.Size; i++)
            {
                for (int j = 0; j < a.Size; j++)
                {
                    matrixMultiplied[i, j] = a[i,j] * x;
                }
            }

            return matrixMultiplied;
        }

        public static IntMatrix operator *(IntMatrix a, IntMatrix b)
        {
            if (a.Size != b.Size)
            {
                throw new MatrixException("Sizes of the matrixes must be equal:", nameof(a.Size) + " and " + nameof(b.Size));
            }
            var matrixMultiplied = new IntMatrix(a.Size);
            for (int i = 0; i < a.Size; i++)
            {
                for (int j = 0; j < a.Size; j++)
                {
                    for (int k = 0; k < a.Size; k++)
                    {
                        matrixMultiplied[i, j] += a[i, k] * b[k, j];
                    }
                }
            }
            return matrixMultiplied;
        }
    }
}
