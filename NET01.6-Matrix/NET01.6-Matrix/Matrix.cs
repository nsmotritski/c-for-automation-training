﻿using System;

namespace NET01._6_Matrix
{
    public class Matrix<T>
    {
        private const int defaultSize = 2;
        public int Size {get;}
        protected int[,] matrix;
        public event EventHandler<ChangeFieldEventArgs> OnChangeField;

        public Matrix (int size = defaultSize)
        {
            if (size <= 0)
            {
                throw new MatrixException("Must be > 0", nameof(size));
            }
            Size = size;
            matrix = new int[Size,Size];
        }

        public int this[int i, int j]
        {
            get
            {
                if (i < 0 || j < 0 || i >= Size || j >= Size)
                {
                    throw new MatrixException("Must be > 0", nameof(i));
                }
                return matrix[i,j];
            }
            set
            {
                if (i < 0 || j < 0 || i >= Size || j >= Size)
                {
                    throw new MatrixException("Must be > 0", nameof(i));
                }
                if (matrix[i, j] == value) return;
                matrix[i, j] = value;
                OnChangeField?.Invoke(this, new ChangeFieldEventArgs(i, j, matrix[i, j], value));
            }
        }

        public override string ToString()
        {
            var result = string.Empty;
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    result += matrix[i, j] + " ";
                }
                result += Environment.NewLine;
            }
            return result;
        }
    }
}
