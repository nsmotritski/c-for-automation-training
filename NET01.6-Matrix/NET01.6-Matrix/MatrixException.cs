﻿using System;

namespace NET01._6_Matrix
{
    class MatrixException : ArgumentException
    {
        public MatrixException(string message, string paramName) : base(message, paramName)
        {
        }
    }
}
