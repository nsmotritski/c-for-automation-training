﻿using System;

namespace NET01._6_Matrix
{
    class Program
    {
        public static void Print(object sender, ChangeFieldEventArgs args)
        {
            Console.WriteLine("Regular method triggered.");
            Console.WriteLine(args.ToString());
        }

        static void Main(string[] args)
        {
            var m1 = new IntMatrix(3);
            m1.PopulateIntMatrix();
            Console.WriteLine("Matrix1:");
            Console.WriteLine(m1.ToString());
            Console.WriteLine();

            var m2 = new IntMatrix(3);
            m2.PopulateIntMatrix();
            Console.WriteLine("Matrix2:");
            Console.WriteLine(m2.ToString());
            Console.WriteLine();

            var sum = new IntMatrix(3);
            sum = m1 + m2;
            Console.WriteLine("Matrix sum:");
            Console.WriteLine(sum.ToString());
            Console.WriteLine();

            Console.WriteLine("Matrix multiplied by int:");
            Console.WriteLine((sum * 3).ToString());
            Console.WriteLine();

            Console.WriteLine("Matrix multiplied by matrix:");
            Console.WriteLine((m1 * m2).ToString());
            Console.WriteLine();

            m1.OnChangeField += Print;
            m1[0, 1] = 13;
            Console.WriteLine("Matrix1:");
            Console.WriteLine(m1.ToString());
            Console.WriteLine();

            m1.OnChangeField -= Print;
            m1.OnChangeField += (sender, eventArgs) =>
            {
                Console.WriteLine("Lambda-expression triggered.");
                Console.WriteLine(eventArgs.ToString());
            };
            m1[1, 0] = 24;
            Console.WriteLine("Matrix1:");
            Console.WriteLine(m1.ToString());
            Console.WriteLine();


            m1.OnChangeField += delegate { Console.WriteLine("Anonymous method called."); };
            m1[1, 1] = 35; // here both lamda-expression and delegate are triggered due to unable to delete lambda
            Console.WriteLine("Matrix1:");
            Console.WriteLine(m1.ToString());
            Console.WriteLine();

            Console.ReadKey();
        }
    }
}
