﻿using System;

namespace NET02._1_SparseMatrix
{
    class MatrixException : ArgumentException
    {
        public MatrixException(string message, string paramName) : base(message, paramName)
        {
        }
    }
}
