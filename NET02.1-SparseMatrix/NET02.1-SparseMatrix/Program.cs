﻿using System;

namespace NET02._1_SparseMatrix
{
    class Program
    {
        static void Main(string[] args)
        {
            var sparseMatrix1 = new SparseMatrix(5);
            sparseMatrix1[1, 2] = 5;
            sparseMatrix1[3, 4] = 27;
            sparseMatrix1[2, 2] = 31;
            sparseMatrix1[3, 3] = 15;
            sparseMatrix1[1, 4] = 5;
            Console.WriteLine(sparseMatrix1.ToString());
            Console.WriteLine();

            Console.WriteLine("Testing Enumerator:");
            foreach (var element in sparseMatrix1)
            {
                Console.WriteLine(element);
            }
            Console.WriteLine();

            Console.WriteLine("Testing Track() method:");
            Console.WriteLine("Sum of diagonal elements of the matrix (Linq): " + sparseMatrix1.Track());
            Console.WriteLine();

            Console.WriteLine("Testing CheckCount() method:");
            Console.WriteLine("Number of elements with value = 5 is (Linq): " + sparseMatrix1.CheckCount(5));
            Console.WriteLine();

            Console.ReadKey();
        }
    }
}
