﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace NET02._1_SparseMatrix
{
    public class SparseMatrix : IEnumerable<int>
    {
        private readonly Dictionary<string, int> _elementsWithValues = new Dictionary<string, int>();
        private int Size { get; }

        public SparseMatrix(int size)
        {
            if (size <= 0)
            {
                throw new MatrixException("Must be > 0", nameof(size));
            }
            Size = size;
        }

        private void ValidateCoordinate(int coordinate, string nameOfCoordinate)
        {
            if (coordinate < 0 || coordinate >= Size)
            {
                throw new MatrixException("Must be > 0 and < Size", nameOfCoordinate);
            }
        }

        private void ValidateCoordinates(int i, int j)
        {
            ValidateCoordinate(i, nameof(i));
            ValidateCoordinate(j, nameof(j));
        }

        private static string GetKey(int i, int j) => $"[{i},{j}]";

        public int this[int i, int j]
        {
            get
            {
                ValidateCoordinates(i, j);
                if (_elementsWithValues.TryGetValue(GetKey(i, j), out int value))
                {
                    return value;
                }
                return default(int);
            }
            set
            {
                if (value != 0)
                {
                    ValidateCoordinates(i, j);
                    _elementsWithValues[GetKey(i, j)] = value;
                }
            }
        }

        public IEnumerator<int> GetEnumerator()
        {
            for (int i = 0; i < Size; i++)
            {
                yield return this[i, i];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public int Track()
        {
            return this.Sum();
        }

        public int CheckCount(int x)
        {
            return x == 0 ? (Size * Size - _elementsWithValues.Count) : _elementsWithValues.Values.Count(element => element == x);
        }

        public override string ToString()
        {
            var result = string.Empty;
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    result += this[i, j] + " ";
                }
                result += Environment.NewLine;
            }
            return result;
        }
    }
}
